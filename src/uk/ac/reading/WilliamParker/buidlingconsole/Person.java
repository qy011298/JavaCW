package uk.ac.reading.WilliamParker.buidlingconsole;

import java.awt.Point;
import java.util.ArrayList;

public class Person {

	private Point pos;
	private ArrayList<Point> Path;

	Person(Point x) { // Just calls the setPos function
		SetPos(x);
	}

	public Point GetPos() { // Return pos of person
		return pos;
	}

	public void SetPos(Point x) { // Set Pos of Person
		pos = x;
	}

	public void showPerson(BuildingInterface bi) {	//places the P on the board
		bi.showIt(pos.x + 1, pos.y + 1, 'P');
	}

	public void setdLoc(Point x) { // sets door location of room person is in.
		
	}

	public void clearPath() {	//if the path has any points, clear it.
		if (Path.size() > 0) {
			Path.clear();
		}
	}

	public void addPathPt(Point x) {	//adds another point to the array
		Path.add(x);
	}
	
	public void remvPathPt() {	//removes the first point in the array
		Path.remove(0);			//puts every other point to the left.
	}
	
	public void createPathvar() {
		Path = new ArrayList<Point>();	//function to create the variable
	}
	
	public int retrnPSize() {	//returns the size of the Path array
		return Path.size();
	}

	public int movePerson() { 	// Moves the person by reading the path
		int dx, dy, xP, yP;
		xP = Path.get(0).x;		//Gets the x coordinate of the next destination
		yP = Path.get(0).y;		//Gets the y one
			dx = xP - pos.x;	//finds the difference in each coord and current position
			dy = yP - pos.y;
			if ((dx == 0) && (dy == 0)) {	//if there's no difference (got to the location)
				remvPathPt();				//remove that path point
				if (Path.size() == 0) return -1;	//if no more points, return -1
				return 0;							//otherwise, return 0
			} else {
				if (dx > 1)			//Code to reduce dx and dy so it moves incrementally
					dx = 1;
				else if (dx < -1)
					dx = -1;
				if (dy > 1)
					dy = 1;
				else if (dy < -1)
					dy = -1;
				pos.translate(dx, dy);	//translate it
				return 0;				//returns 0;
			}
	}

	public String toString() {
		return "Person is in " + pos.x + "," + pos.y + ".\n";
	}

}
