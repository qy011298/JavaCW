package uk.ac.reading.WilliamParker.buidlingconsole;

import java.util.Scanner;

import java.util.concurrent.TimeUnit;

import javax.swing.JFileChooser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class BuildingInterface {

	Scanner s, s2; // scanner used for input from user
	char[][] board;

	Building myBuilding; // building in which rooms are shown

	/**
	 * return as String definition of bOpt'th building
	 * 
	 * @param bOpt
	 */
	public String buildingString(int bOpt) {
		if (bOpt == 1)
			return "11 11;0 0 4 4 2 4;6 0 10 10 6 5;0 6 4 10 2 6";
		else
			return "40 12;0 0 15 4 8 4;15 0 30 4 22 4;0 6 10 11 6 6";
	}

	public void doDisplay() {
		for (int x = 0; x < (myBuilding.getY() + 2); x++) { // the +2 accounts for the boarders
			System.out.println();					//starts new line (to show it as 2D)
			for (int y = 0; y < (myBuilding.getX() + 2); y++)	
				System.out.print(board[y][x]);		//prints the board
		}
		System.out.println();					//Formatting
	}

	private void initBoard() { 					// Initialises board
		board = new char[myBuilding.getX() + 2][myBuilding.getY() + 2]; // makes the board based on size of building.
		int tempx = (myBuilding.getX() + 2);	//+2 to account for boarders
		int tempy = (myBuilding.getY() + 2);
		for (int x = 0; x < tempx; x++) { 		// filling x array
			board[x][0] = '#';
			board[x][tempy - 1] = '#';
		}
		for (int y = 0; y < tempy; y++) { 		// filling y array
			board[0][y] = '#';		
			board[tempx - 1][y] = '#';
		}
	}

	public void showIt(int x, int y, char ch) {
		if (board[x][y] == 'D') {			
			board[x][y] = '&';				//Shows an & when a person is on top of a door
		} else
			board[x][y] = ch;				//Places ch into wherever the board is
	}	

	public void showWall(int x1, int x2, int y1, int y2, int xd, int yd) {	//passes in all the variables of the walls
		for (int temp = x1; temp <= x2; temp++) {		
			showIt(temp + 1, y1 + 1, '-');			//Drawing the top wall
		}
		for (int temp = y1; temp <= y2; temp++) {	//Drawing the left wall
			showIt(x1 + 1, temp + 1, '|');
		}
		for (int temp = x1; temp <= x2; temp++) {	//Drawing the bottom wall
			showIt(temp + 1, y2 + 1, '-');
		}
		for (int temp = y1; temp <= y2; temp++) {	//Drawing the right wall
			showIt(x2 + 1, temp + 1, '|');
		}
		showIt(xd + 1, yd + 1, 'D');				//Places a "D" to represent the door
	}

	public void animate() { // animate function
			while (myBuilding.mPerson() == 0) { 	// While the person's path isn't complete
				initBoard(); 						// initialise board
				myBuilding.showBuilding(this);		// update the building
				doDisplay(); 						// show display
				try {
					TimeUnit.MILLISECONDS.sleep(250);// Sleeps for 250 ms
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
	}
	
	public void newBuilding() {
		s2 = new Scanner(System.in);	//defining new scanner for user input
		System.out.println("Enter the x size for the building");
		int xSize = s.nextInt();			//Finding x size
		System.out.println("Enter the y size for the building");
		int ySize = s.nextInt();			//Finding y size
		
		System.out.println("Enter in the room strings, rooms seperated by commas");
		String bString = xSize + " " + ySize +";" + s2.nextLine();	//Puts the string in correct format
		myBuilding = new Building(bString);						//Makes the building
	}
	
	public void saveBuilding() {
	    JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new File(System.getProperty("user.home")));	//sets the directiry
	    int retrival = chooser.showSaveDialog(null);			
	    if (retrival == JFileChooser.APPROVE_OPTION) {			//If a file is selected
	        try {
	            FileWriter fw = new FileWriter(chooser.getSelectedFile()+".txt");
	            fw.write(myBuilding.returnBString());			//Function to return the building string
	            fw.close();										//Closes the filewriter
	        } catch (Exception ex) {							//In case of filewriter throwing an exception
	            ex.printStackTrace();
	        }
	       
	    }
	}
	
	public void loadBuilding() throws Exception {			//In case the File reader doesn't work
		String bString = " ";
		JFileChooser chooser = new JFileChooser();			//creates a new file chooser
		chooser.setCurrentDirectory(new File(System.getProperty("user.home")));	//sets directory (for ease)
	    int returnVal = chooser.showOpenDialog(null);		//finds if file was selected or not
	    if(returnVal == JFileChooser.APPROVE_OPTION) {		//if file is selected
	      File file = chooser.getSelectedFile();			//Puts it in a variable
			Scanner sLB = new Scanner(new FileReader(file));//scanner to read the file
	    	bString = sLB.nextLine();						//assigns the first string to variable b string
	    	myBuilding = new Building(bString);				//Puts it as the active building
	    	sLB.close();									//close the scanner.
	    }
	}

	/**
	 * constructor for BuildingInterface sets up scanner used for input and the
	 * arena then has main loop allowing user to enter commands
	 */
	public BuildingInterface() {
		s = new Scanner(System.in); // set up scanner for user input
		int bno = 2; // initially building 1 selected

		myBuilding = new Building(buildingString(bno));// create building
		initBoard();

		char ch = ' ';
		do {
			System.out.print("(N)ew buidling, (S)ave building, (L)oad building, (D)raw, (I)nfo, (M)ove, (A)nimate,\n(P)ath, e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'N':
			case 'n':
				newBuilding();		//Calls the newBuilding function
				initBoard();
				System.out.println("New Building Created");	
				break;
			case 'I':
			case 'i':
				System.out.print(myBuilding.toString());
				break;
			case 'x':
			case 'X':
				ch = 'X'; 			// when X detected program ends
				break;
			case 'd':
			case 'D': 				// updates the board and draws it
				initBoard();
				myBuilding.showBuilding(this);
				doDisplay();
				break;
			case 'm': // moves the person, updates and draws
			case 'M':
				initBoard();
				myBuilding.AddPath(0);	//Adds path, using the rule 0.
				myBuilding.mPerson();	//Move person function
				myBuilding.showBuilding(this);	
				doDisplay();					//Shows the building
				break;
			case 'a': // calls animation method.
			case 'A':
				myBuilding.AddPath(0);	//Add's the path using rule 0
				animate();				//animates it using the array of points
				break;
			case 'p':
			case 'P':
				myBuilding.AddPath(1);	//adds the paths using rule 1
				animate();				//animates it using the array of points
				break;
			case 's':
			case 'S':
				saveBuilding();
				break;
			case 'l':
			case 'L':
				try {
					loadBuilding();			//Calls the load building function
				} catch (Exception e) {		//If there's an error, print that
					System.out.println("There's an issue, Please try a different file");
				}
				break;
			}
		} while (ch != 'X'); // test if end

		s.close(); // close scanner
	}

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		BuildingInterface b = new BuildingInterface();	//builds the building interface
	}
}
