package uk.ac.reading.WilliamParker.buidlingconsole;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class Building {

	private int xSize = 10, ySize = 10;	// size of building in x and y
	private ArrayList<Room> allRooms;	// array of rooms
	private String[] Info, BSize;	
	private Person person;
	
	Building(String S){
		setBuilding(S);
		setPerson();
	}
	
	public void setPerson() {
		int rNo;   											//room number the person is in.
		Random randGen = new Random();						//Making a random object
		int room = randGen.nextInt(allRooms.size()) ;		//Generating random number based on no.ofRooms
		Point P = allRooms.get(room).getRandom(randGen);	//making a random point from that.
		person = new Person(P);								//Making the person
		rNo = personRoom();
		if (rNo != -1) person.setdLoc(allRooms.get(rNo).returnDoor(0));		//if the person is in a room, set the door
																			//location variable to the point of the door
	}
	
	public int mPerson() {		//Move person function
		int x;
			x = person.movePerson();	//will return a value of 0 or -1
			if (x != 0) {				//-1 means the path has been finished.
				System.out.println("Person has finished their path\n");
				return -1;		//returns -1 to the "animate" function		
			}	
			else return 0;  //returns a 0 to the animate function
		
	}
	
	public int rtnPathSize() {		//simple function to give the size of the path
		return person.retrnPSize();
	}
	
	public void AddPath(int x) {	//adds a path to the person. int x refers to the rule set
		int currm = personRoom();	//Current Room number
		int newrm = randRoom();		//Random Room Nymber
		
		if (x == 1) {				//Using rule 1 - if there's multiple points in the path
		while (currm == newrm) {	//if the randomly selected room is the same as the current one, pick another
			newrm = randRoom();
		}
		person.createPathvar();			//Create the person 
		person.clearPath();				//Clear the Path
		person.addPathPt(allRooms.get(currm).returnDoor(-1));	//Add the point before the door of current room.
		person.addPathPt(allRooms.get(currm).returnDoor(1));	//Add the point after the door of current room.
		person.addPathPt(allRooms.get(newrm).returnDoor(1));	//Add the point after the door of the new room.
		person.addPathPt(allRooms.get(newrm).returnDoor(-1));}	//Add the point before the door of the new room.
		else {						//Using rule 0 - If there's a single point.
			person.createPathvar();	
			person.clearPath();
			person.addPathPt(allRooms.get(currm).returnDoor(-1)); //Adds a point of the door of the current room.
			person.addPathPt(allRooms.get(currm).returnDoor(0));
		}
	}

	public int getX(){	//Get size of building in x coordinate
		return xSize;
	}
	
	public int getY(){	//Get size of building in y coordinate
		return ySize;
	}
	
	public void setBuilding(String bS) {					//Building constructor
	allRooms = new ArrayList<Room>();
		allRooms.clear(); 									//clear the current array list
		Info = bS.split(";");								//split bS by ";"
		BSize = Info[0].split(" ");							//use first string to set xSize and ySize
		xSize = Integer.parseInt(BSize[0]);					//for all the remaining strings
		ySize = Integer.parseInt(BSize[1]);
		for (int x = 1; x < Info.length; x++){
			allRooms.add(new Room(Info[x]));				//Does a for loop to add the room to the arraylist.
		}
		setPerson();										//Sets the person
		
	}
	
	public int personRoom() {								//Function to find the room the person is in.
		for (int x = 0; x<allRooms.size(); x++) {			//steps through each room
			if ((allRooms.get(x).isInRoom(person.GetPos())) == true) return x;	//if "isInRoom" is true, return the room no.
		};
		return -1;	//otherwise, return -1.
	}
	
	public String toString(){									//A print function. Uses recursion to complete
		String s = "";
		s = s + "Building Size " + xSize + "," + ySize +"\n";
		for (Room r : allRooms) s = s + r.toString() + "\n";	//Prints from the array list.
		s = s + person.toString();
		s = s + "\nPerson is in room " + (personRoom()+1 + "\n");	//Rooms random from 1 - n;
		return s;
	}
	
	public int randRoom() {		//Function to randomly generate a room of the building.
		Random randGen = new Random();	//creates new variable
		return randGen.nextInt(allRooms.size());	//finds random number
	}
	
	public String returnBString() {
		String s = "";
		s = xSize + " " + ySize +";";
		for (Room r : allRooms) s = s + r.returnRString();
		return s;
	}
	
	
	
	/**
	 * show all building�s rooms and occupants in the interface
	 * @param bi	the interface
	 */
	public void showBuilding (BuildingInterface bi) {
		for (int x = 0; x<allRooms.size(); x++) {
			allRooms.get(x).showRoom(bi);
		}
		person.showPerson(bi);
	}

	
	public static void main(String[] args) {
		Building b = new Building("11 11;0 0 5 5 3 5;6 0 10 10 6 6;0 5 5 10 2 5");
		
		System.out.println(b.toString());
	}
}
