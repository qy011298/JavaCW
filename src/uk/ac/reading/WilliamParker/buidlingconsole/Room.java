package uk.ac.reading.WilliamParker.buidlingconsole;

import java.awt.Point;
import java.util.Random;

public class Room {
	
	private String[] coOrds;	
	private int x1,x2,y1,y2,xd,yd,dSize;	
	private int[] intcoOrds = {0,0,0,0,0,0,0};

	Room(String s) {
    	coOrds = s.split(" ");		// split the strings
    	for (int x = 0; x < coOrds.length; x++){
    		intcoOrds[x] = Integer.parseInt(coOrds[x]);
    	}
    	x1 = intcoOrds[0];	//Storing the integers for the dimensions of the room
    	x2 = intcoOrds[2];
    	y1 = intcoOrds[1];
    	y2 = intcoOrds[3];
    	xd = intcoOrds[4];	//xd and yd refer to the coordinates of the door
    	yd = intcoOrds[5];
    	if (intcoOrds[6] == 0) dSize = 1;
    	else dSize = intcoOrds[6];		//if the size is specified. if not, it's size 1.
    }
		
	public String toString() {	//simple function to print out info about the room
	    	String res = "";
	    	res = res + "Room from (" + x1 + "," + y1 + ") to (" + x2 + "," + y2 
	    			+") Door at ("+ xd + ","+ yd + ") " +" size:" + dSize;
	    	return res;
	 }
	
	public String returnRString() {
		String rm = "";
		rm = x1 + " " + y1 + " " + x2 + " " + y2 + " " + xd + " " + yd +";";
		return rm;
	}
	 
	 public boolean isInRoom(Point P){
		 int tX, tY; //Temporary x and temporary y
		 tX = (int) P.getX();	//finds the x and y coordinate of the person
		 tY = (int) P.getY();	
		 
		 if ((tX >= x1) && (tX <= x2) && (tY >= y1) && (tY <= y2)){	//if the points fall in the correct range
			 return true;		
		 }
		 else return false;
		 
	 }
	 
	 public void showRoom(BuildingInterface bi) {	//Pass through function
		 bi.showWall(x1,x2,y1,y2,xd,yd);
	 }
	 
	 public Point getRandom(Random O) {	 //makes 2 random integers and puts it into a point
		 int valx, valy;
		 valx = x1 + 1 + O.nextInt(x2-x1-2);
		 valy = y1 + 1 + O.nextInt(y2-y1-2);
		 
		 Point P = new Point(valx,valy);
		 return P;
	 }
	 
	 public Point returnDoor(int loc) {		//Function to return coordinates of the door and around it. 
		 Point Q = new Point(xd,yd);
		 if (xd == x1) {			//if door is on left wall
			 if (loc == 1) {		//Outside door
				 Point p = new Point(xd-1,yd);
				 return p;			//returns the point just outside the door
			 }
			 else if (loc == -1) {	//inside door
				 Point p = new Point(xd+1,yd);
				 return p;			//Returns the point just inside the door
			 }
			 else return Q;			//Returns the door point
		 }
		 else if (xd == x2) {			//if door is on right wall
			 if (loc == 1) {		//Outside door
				 Point p = new Point(xd+1,yd);
				 return p;
			 }
			 else if (loc == -1) {	//Inside door
				 Point p = new Point(xd-1,yd);
				 return p;
			 }
			 else return Q;
		 }
		 else if (yd == y1) {			//if door is on top wall
			 if (loc == 1) {		//Outside door
				 Point p = new Point(xd,yd-1);
				 return p;
			 }
			 else if (loc == -1) {	//inside door
				 Point p = new Point(xd,yd+1);
				 return p;
			 }
			 else return Q;
		 }
		 else if (yd == y2) {			//if door is on bottom wall
			 if (loc == 1) {		//outside door
				 Point p = new Point(xd,yd+1);
				 return p;
			 }
			 else if (loc == -1) {	//inside door
				 Point p = new Point(xd,yd-1);
				 return p;
			 }
			 else return Q;
		 }
		 return Q;
	 }
	 	
	public static void main(String[] args) {	
	Room r = new Room("0 0 5 5 0 2 6");	//splitting strings using space
	Point P = new Point(5,5);
	System.out.println(r.isInRoom(P));	//prints the boolean variable if the variable is in the room
	System.out.println(r.toString());
	System.out.println(r.returnRString());
	
	}

}
